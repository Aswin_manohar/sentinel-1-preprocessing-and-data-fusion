# Sentinel-1 processing and data fusion

This project collects our work on processing sentinel-1 to classify tree species by data fusion.
## Minimum viable product 

TBD..

## Usage

Dependencies are listed in environment.yml and can be installed using conda
```sh
$ conda env create -f environment.yml
```
Activate the new environment:
```sh 
conda activate species_classification
```
Verify that the new environment was installed correctly:
```sh 
conda env list
```
